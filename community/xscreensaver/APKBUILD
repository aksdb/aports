# Contributor: prspkt <prspkt@protonmail.com>
# Maintainer: prspkt <prspkt@protonmail.com>
pkgname=xscreensaver
pkgver=6.02
pkgrel=0
pkgdesc="Screensavers for X11 environment"
url="https://www.jwz.org/xscreensaver/"
arch="all"
license="GPL-2.0-or-later GPL-3.0-or-later 0BSD MIT"
depends="bc"
options="suid"
makedepends="gtk+-dev libjpeg-turbo-dev mesa-dev gettext-dev libxmu-dev perl
	libxml2-dev libxi-dev libxinerama-dev libxrandr-dev glu-dev gdk-pixbuf-xlib-dev
	"
subpackages="$pkgname-doc $pkgname-gl-extras:gl $pkgname-extras $pkgname-lang"
source="https://www.jwz.org/xscreensaver/xscreensaver-$pkgver.tar.gz"

_libexecdir=/usr/lib/xscreensaver
_confdir=/usr/share/xscreensaver/config

# secfixes:
#   6.02-r0:
#     - CVE-2021-34557

build() {
	# https://bugs.gentoo.org/830914
	[ "$CARCH" = "aarch64" ] && export CFLAGS="$CFLAGS -flax-vector-conversions"

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--with-gtk \
		--libexecdir=$_libexecdir \
		--with-xinerama-ext \
		--with-xf86vmode-ext \
		--with-randr-ext
	make
}

package() {
	make install_prefix="$pkgdir" install
}

gl() {
	local _conf
	pkgdesc="An enhanced set of screensavers that require OpenGL"
	cd "$pkgdir"
	mkdir -p "$subpkgdir"/$_confdir
	scanelf -Rn . | awk '$2 ~ /libGL/ { print $3}' | while read -r f; do
		mkdir -p "$subpkgdir"/${f%/*}
		mv "$f" "$subpkgdir"/${f%/*}

		_conf=usr/share/xscreensaver/config/${f##*/}.xml
		if [ -f "$_conf" ]; then
			mv "$_conf" "$subpkgdir"/$_confdir
		fi
	done
}

extras() {
	pkgdesc="An enhanced set of screensavers"
	mkdir -p "$subpkgdir"/$_libexecdir \
		"$subpkgdir"/$_confdir
	mv "$pkgdir"/$_libexecdir/* "$subpkgdir"/$_libexecdir
	mv "$pkgdir"/$_confdir/* "$subpkgdir"/$_confdir
}

sha512sums="
2291ec6ca2d2a24dae975f7f3a8e1733c06f289eb74955db5b3344c7ddcc1d72f82d380df984ef9199f2ed7ab8a7bc920da57d98f589ae5fd1cee082755ba1ff  xscreensaver-6.02.tar.gz
"

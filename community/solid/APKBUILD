# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=solid
pkgver=5.91.0
pkgrel=0
pkgdesc="Hardware integration and detection"
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="qt5-qtdeclarative-dev"
makedepends="$depends_dev
	extra-cmake-modules
	bison
	doxygen
	eudev-dev
	flex-dev
	qt5-qttools-dev
	udisks2-dev
	upower-dev
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/solid-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# solidmttest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "solidmttest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
381cfd658b5cf8a6448f42bbc5e43122d7d2438facd7e33ae9f8346b258e7ea00838b2b317594403f34b2c12ca62eef8b9c3f82126453027a33556fa12a58450  solid-5.91.0.tar.xz
"

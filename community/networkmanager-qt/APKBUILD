# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=networkmanager-qt
pkgver=5.91.0
pkgrel=0
pkgdesc="Qt wrapper for NetworkManager API"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> networkmanager
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="networkmanager"
depends_dev="networkmanager-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/networkmanager-qt-$pkgver.tar.xz"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# The excluded tests currently fail
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(manager|settings|activeconnection)test'
}


package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
2a9570a86c15f587f17edc5a14f1a3e0f8e44e504d16174d5fb2d15e85f82ba785eefd52a40e67bb9d8b310dc74f76678333fc06082974e8eaa408ceedb2ebae  networkmanager-qt-5.91.0.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcalendarcore
pkgver=5.91.0
pkgrel=0
pkgdesc="The KDE calendar access library"
options="!check" # RecursOn-ConnectDaily(2|3|6) make the builders stuck
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later"
depends_dev="qt5-qtbase-dev libical-dev"
makedepends="$depends_dev extra-cmake-modules doxygen qt5-qttools-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcalendarcore-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

replaces="kcalcore"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# testrecurtodo, testreadrecurrenceid, testicaltimezones, testmemorycalendar and testtimesininterval are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "test(recurtodo|readrecurrenceid|icaltimezones|memorycalendar|timesininterval)"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="
221a652653189d5e2733e02f54c2b4ec7d7450fe0b8666736fb16764f4491975bfa0d5569b5919832f074a10d6ecba2127f62d0e0215b16b4c219381ea58328c  kcalendarcore-5.91.0.tar.xz
"

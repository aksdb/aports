# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-firewall
pkgver=5.24.1
pkgrel=0
pkgdesc="Control Panel for your system firewall"
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> kcmutils
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="
	python3
	kirigami2
	"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kcoreaddons-dev
	kdeclarative-dev
	ki18n-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtx11extras-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-firewall-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
8d3d0ed7a07ea3c9a0e23d9b3965f6e800c13b14188ad29e3201e88daa72ceee348ea84d8ca18be128fd1836502676168719d4a0f91e3092d57254fa14075c69  plasma-firewall-5.24.1.tar.xz
"

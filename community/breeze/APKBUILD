# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=breeze
pkgver=5.24.1
pkgrel=0
pkgdesc="Artwork, styles and assets for the Breeze visual style for the Plasma Desktop"
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> kconfigwidgets
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends_dev="
	kconfigwidgets-dev
	kdecoration-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kpackage-dev
	kwindowsystem-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/breeze-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
6dabd5c5dfcc253e3fcbf081c9fcdbd151ffcd1bb0911d21eff34b1bc45e4d185e872883e1d82d97650cec97045d409009cc960f02dc6296eb132e792d5801b8  breeze-5.24.1.tar.xz
"

# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=faudio
pkgver=22.02
pkgrel=0
pkgdesc="Accuracy-focused XAudio reimplementation for open platforms"
url="https://fna-xna.github.io/"
arch="all"
license="Zlib"
makedepends="cmake sdl2-dev"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/FNA-XNA/FAudio/archive/$pkgver.tar.gz"
builddir="$srcdir/FAudio-$pkgver"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	# XXX: For some reason, faudio is not capable of autodetecting
	# the SDL2 build configuration using CMake, just hardcode paths
	# for now to unblock the builders.
	cmake -B build \
		-DSDL2_INCLUDE_DIRS=/usr/include/SDL2 \
		-DSDL2_LIBRARIES=libSDL2.so \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=1 \
		-DBUILD_TESTS=1
	make -C build
}

check() {
	cd build
	./faudio_tests
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="
d16e67709f5a224a62c59091d003ad8c7f07c472533aeb8f9af6b264a4137cba9675023e32bb595465d46ff706c8837b83c309824e5141d2bb839c60e7d97347  faudio-22.02.tar.gz
"

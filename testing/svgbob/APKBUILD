# Contributor: Erwan Rouchet <lucidiot@brainshit.fr>
# Maintainer: Erwan Rouchet <lucidiot@brainshit.fr>
pkgname=svgbob
pkgver=0.5.5
pkgrel=0
pkgdesc="Convert your ascii diagram scribbles into happy little SVG"
url="https://github.com/ivanceras/svgbob"
arch="all !s390x !riscv64" # limited by rust/cargo
license="Apache-2.0"
makedepends="cargo"
source="$pkgname-$pkgver.tar.gz::https://github.com/ivanceras/svgbob/archive/$pkgver.tar.gz"

prepare() {
	default_prepare
	# no --locked because no Cargo.lock has been committed in the source repo
	# https://github.com/ivanceras/svgbob/issues/88
	cargo fetch
}

build() {
	cargo build --frozen --release
}

check() {
	cargo test all --frozen
}

package() {
	install -Dm0755 target/release/svgbob "$pkgdir"/usr/bin/svgbob
}

sha512sums="
40988edc7076226d393e35773ca6fc744ef471b9c0c183551dd69876d91b57978485b5e767f2f4477ed118d65c08935997450d5c5965ffaf4e4ad4935c32ed9a  svgbob-0.5.5.tar.gz
"
